// 

const _ = require ('lodash')

// poin 1, cek string or int
function checkType(cekVar){
    return _.isString(cekVar) ? 'String' : 'Not String'
}

function checkInt(cekVar){
    return _.isInteger(cekVar) ? 'Integer' : 'Not Integer'
}

// poin 2 cetak camel case atau capitalize
function toCamelCase(stringToConvert){
    return _.camelCase(stringToConvert)
}

function toCapitalize(stringToConvert){
    return _.capitalize(stringToConvert)
}



let cekString = 'kentang Goreng'
let cekArray = ['1','2','3']

console.log(checkType('kentang'))
console.log(toCamelCase(cekString))
console.log(toCapitalize(cekString))