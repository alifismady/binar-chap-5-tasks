
function luasLingkaran(jariJari){
    return jariJari * jariJari * 3.14
}

function kelilingLingkaran(jariJari){
    return 2 * jariJari * 3.14
}

module.exports = {
    kelilingLingkaran,
    luasLingkaran
}